var express = require("express");
var router = express.Router();
const pool = require("../config/database.js");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
const privateKey = "code_$secr3T!";

router.post("/token", async (req, res) => {
  const { username, password } = req.body;
  const result = await pool.query(
    `SELECT * FROM user WHERE ueser_name = '${username}'`
  );
  console.log(result[0]);
  console.log(result[0][0]);

  if (result[0].length > 0) {
    const passwordMatch = await bcrypt.compare(password, result[0][0].password);
    if (passwordMatch) {
      const token = jwt.sign(
        {
          id: result[0][0].id,
          username: result[0][0].username,
        },
        privateKey,
        { expiresIn: "90000ms" }
      );
      const result2 = await pool.query(
        `SELECT * FROM user WHERE id = '${result[0][0].id}'`
      );
      res.json({ status: "ok", token: token, user_id: result2[0][0].user_id });
    } else {
      res.status(401).send({ error: "password does not match" });
      return;
    }
  } else {
    res.status(401).send({ error: "user not found" });
    return;
  }
});

module.exports = router;
