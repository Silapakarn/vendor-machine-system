const express = require("express")
const router = express.Router()
const pool = require("../config/database.js")

router.put('/coffee/:id', async (req, res) => {
    const { id, stock } = req.body
    const result = await pool.query(`update coffee set stock = ${stock} where id = ${id}`)    
    res.json(result[0])
    console.log(result[0])
})

router.post('/check-stock/:id', async (req, res) => {
    const id  = req.params.id
    const { quantity } = req.body;
    const stockFromDatabase = await pool.query(`select stock from coffee where id = ?`, [id])
    const currentQuantity = stockFromDatabase[0][0].stock;
    if(currentQuantity === 0){
        pool.query(`update coffee set out_of_stock = 1 where id = ?`, [id])
        res.status(400).send('Insufficient stock.');
        return;
    }else if(currentQuantity > parseInt(quantity)){
        const order =  currentQuantity - parseInt(quantity)
        pool.query(`update coffee set stock = ? where id = ?`, [order,id])
        res.status(200).send('update stock succeed!');
        return;
    }else{
        res.status(400).send(' The number of orders exceeded the stock.');
        return;
    }
})

router.put('/tea/:id', async (req, res) => {
    const { id, stock } = req.body
    const result = await pool.query(`update tea set stock = ${stock} where id = ${id}`)
    res.json(result[0])
    console.log(result[0])
})

router.put('/soft_drink/:id', async (req, res) => {
    const { id, stock } = req.body
    const result = await pool.query(`update soft_drink set stock = ${stock} where id = ${id}`)
    res.json(result[0])
    console.log(result[0])
})




module.exports = router