const express = require("express");
const router = express.Router();
const pool = require("../config/database.js");
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();
const bcrypt = require("bcrypt");
const saltRounds = 10;
var jwt = require("jsonwebtoken");

router.post("/registers", jsonParser, (req, res, next) => {
  bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
    pool.query(
      "INSERT INTO user (ueser_name, password, email) VALUES (?, ?, ?)",
      [req.body.username, hash, req.body.email],
      function (err, results, fields) {
        if (err) {
          res.json({ status: "error", massage: err });
          return;
        }
        res.json({ status: "ok" });
      }
    );
  });
});

router.post("/login", jsonParser, function (req, res, next) {
  pool.query(
    "SELECT * FROM user WHERE ueser_name =?",
    [req.body.username],
    function (err, users, fields) {
      if (err) {
        res.json({ status: "error", message: err });
        return;
      }
      if (users.length == 0) {
        res.json({ status: "error", message: "This user has no information in the system." });
        return;
      }
      bcrypt.compare(
        req.body.password,
        users[0].password,
        function (err, isLogin) {
          if (isLogin) {
            var token = jwt.sign({ username: users[0].ueser_name }, secret, {
              expiresIn: "1h",
            });
            res.json({ status: "ok", message: "login success", token });
          } else {
            res.json({ status: "error", message: "login failed" });
          }
        }
      );
    }
  );
});

module.exports = router;
