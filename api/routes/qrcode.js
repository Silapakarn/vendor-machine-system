const express = require("express")
const router = express.Router()
const pool = require("../config/database.js")


router.post('/', async (req, res) => {
    try {
        const { number_of_user_scanners_qr_code }  = req.body    //1
        const qrCodeFromDatabase = await pool.query(`SELECT number_of_user_scanners_qr_code FROM mst_qr_code`)
        const qrCodeForCalculate = qrCodeFromDatabase[0][0].number_of_user_scanners_qr_code //100
        const result = number_of_user_scanners_qr_code + qrCodeForCalculate; // 1 + 100 = 101
        pool.query(`update mst_qr_code set number_of_user_scanners_qr_code = ${result} `)
        res.status(200).send('update QR Code succeed!');
    } catch(e) {
        console.log(e);
        res.status(401).send('update QR Code Fail..')
    }
})


module.exports = router