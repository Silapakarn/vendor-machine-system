const express = require("express");
const router = express.Router();
const pool = require("../config/database.js");

router.get("/config_beverage", async (req, res) => {
  try {
    const results = await pool.query(`select * from mst_config`);
    res.status(200).json(results[0]);
  } catch (e) {
    res.status(400).json({ message: "Error get data from Database. " });
  }
});

router.get("/config_beverage/0", async (req, res) => {
  try {
    const results = await pool.query(
      `select * from mst_config where config_group = 'COFFEE'`
    );
    res.status(200).json(results[0]);
  } catch (e) {
    res.status(400).json({ message: "Error get data from Database. " });
  }
});

router.get("/config_beverage/1", async (req, res) => {
  try {
    const results = await pool.query(
      `select * from mst_config where config_group = 'TEA'`
    );
    res.status(200).json(results[0]);
  } catch (e) {
    res.status(400).json({ message: "Error get data from Database. " });
  }
});

router.get("/config_beverage/2", async (req, res) => {
  try {
    const results = await pool.query(
      `select * from mst_config where config_group = 'SOFT_DRINK'`
    );
    res.status(200).json(results[0]);
  } catch (e) {
    res.status(400).json({ message: "Error get data from Database. " });
  }
});

router.post("/update_beverage", async (req, res) => {
  try {
    const {
      type_coffee,
      price_coffee,
      duration_coffee,
      type_tea,
      price_tea,
      duration_tea,
      type_soft_drink,
      price_soft_drink,
      duration_soft_drink,
    } = req.body;
      await pool.query(`UPDATE coffee SET price = ?, duration = ? WHERE type = ?`,[price_coffee, duration_coffee, type_coffee])
      await pool.query(`UPDATE tea SET price = ?, duration = ? WHERE type = ?`, [price_tea, duration_tea, type_tea])
      await pool.query(`UPDATE soft_drink SET price = ?, duration = ? WHERE type = ?`, [price_soft_drink, duration_soft_drink, type_soft_drink])
      res.status(200).json({message: "Update config beverage successful. "})  
  } catch (e) {
    res.status(400).json({ message: "Error update config beverage. " });
  }
});

module.exports = router;
