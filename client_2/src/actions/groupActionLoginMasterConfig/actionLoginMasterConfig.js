import { fetchLoginMasterConfig } from '../../api/groupLoginMasterConfig'
import store from '../../modules/loginMasterConfig'

const getLoginMasterConfigList = (params) => {
    const callAction = async (dispatch) => {
      dispatch(
        store.set({
          key: 'loginMasterConfig',
          value: [],
        })
      )
      const {
        data
      } = await fetchLoginMasterConfig(params)
      dispatch(
        store.set({
          key: 'loginMasterConfig',
          value: data,
        })
      )
      dispatch(
        store.set({
          key: 'total',
          value: data.totalElements,
        })
      )
    }
  
    return {
      loadingMessage: 'Fetching data..',
      successMessage: 'Success',
      callAction,
    }
  }
  
  export { getLoginMasterConfigList }


