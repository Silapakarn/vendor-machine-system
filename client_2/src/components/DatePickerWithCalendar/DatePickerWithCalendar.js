import styled from "@emotion/styled";
import React, { useState, useEffect } from "react";
import {
  Modal,
  Calendar,
  Input,
  Row,
  Col,
  Select,
  Typography,
  Button,
} from "antd";
import moment from "moment";
import { CalendarOutlined } from "@ant-design/icons";
import "moment/locale/th";
import locale from "antd/es/date-picker/locale/th_TH";

const DatePickerWithCalendar = ({
  label,
  disabledDate = () => false,
  mode = "month",
  onChange,
  value,
  allowClear = false,
  displayOnly = false,
  disabled = false,
  ...props
}) => {
  const [visible, setVisible] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [dateForCheckInvalid, setDateForCheckInvalid] = useState("");
  const denyTooltip = () => {
    const dateElement = document.getElementsByClassName(
      "ant-picker-cell-in-view"
    );
    for (const element of dateElement) {
      element.removeAttribute("title");
    }
    return null;
  };
  const handleChangeFromText = ({ target: { value } }) => {
    if (mode === "month") {
      const lastChar = value.charAt(value.length - 1);
      let rawData = value.replaceAll("/", "");
      const oldData = inputValue && inputValue.replaceAll("/", "");

      if (rawData === oldData && lastChar !== "/")
        rawData = rawData.slice(0, -1);

      if (rawData && !/\d$/.test(rawData)) {
        return null;
      }
      const d = `${rawData[0] || ""}${rawData[1] ? `${rawData[1]}/` : ""}`;
      const m = `${rawData[2] || ""}${rawData[3] ? `${rawData[3]}/` : ""}`;
      const y = `${rawData[4] || ""}${rawData[5] || ""}${rawData[6] || ""}${
        rawData[7] || ""
      }`;
      const formatValue = `${d}${m}${y}`;
      setInputValue(formatValue);

      const stringToDate =
        formatValue.length === 10
          ? moment(formatValue, "DD/MM/YYYY").add(-543, "y")
          : { _isValid: false };
      if (stringToDate._isValid) {
        const isDisabled = disabledDate(stringToDate);
        if (isDisabled) {
          setInputValue(null);
        } else onChange(stringToDate);
      } else {
        onChange(null);
      }
    } else if (mode === "year") {
      const lastChar = value.charAt(value.length - 1);
      let rawData = value.replaceAll("/", "");
      const oldData = inputValue && inputValue.replaceAll("/", "");

      if (rawData === oldData && lastChar !== "/")
        rawData = rawData.slice(0, -1);

      if (rawData && !/\d$/.test(rawData)) {
        return null;
      }
      const m = `${rawData[0] || ""}${rawData[1] ? `${rawData[1]}/` : ""}`;
      const y = `${rawData[2] || ""}${rawData[3] || ""}${rawData[4] || ""}${
        rawData[5] || ""
      }`;
      const formatValue = `${m}${y}`;
      setInputValue(formatValue);

      const stringToDate =
        formatValue.length === 7
          ? moment(formatValue, "MM/YYYY").add(-543, "y")
          : { _isValid: false };
      if (stringToDate._isValid) {
        const isDisabled = disabledDate(stringToDate);
        if (isDisabled) {
          setInputValue(null);
        } else onChange(stringToDate);
      } else {
        onChange(null);
      }
    }
  };

  const handleOnBlur = () => {
    let valueDate;
    if (mode === "month") {
      valueDate = moment(inputValue, "DD/MM/YYYY");
    } else {
      valueDate = moment(inputValue, "MM/YYYY");
    }
    const isDisabled = disabledDate(valueDate.add(-543, "y"));

    if (!valueDate._isValid || isDisabled) {
      setInputValue("");
      return null;
    }
  };

  const getCSSDisplayOnly = () => {
    if (displayOnly) return "input-display-only";
    return "";
  };

  const handleKeyDown = ({ keyCode }) => {
    if (keyCode === 13) {
      let valueDate;
      if (mode === "month") {
        valueDate = moment(inputValue, "DD/MM/YYYY");
      } else {
        valueDate = moment(inputValue, "MM/YYYY");
      }
      const isDisabled = disabledDate(valueDate.add(-543, "y"));

      if (!valueDate._isValid || isDisabled) {
        setInputValue("");
        return null;
      }
    }
  };

  const handleCloseModal = () => {
    if (mode === "month") {
      const valueToForm = inputValue
        ? moment(inputValue, "DD/MM/YYYY").add(-543, "y")
        : null;
      const invalidDate = disabledDate(moment(dateForCheckInvalid));
      console.log("invalidDate", invalidDate);
      console.log("valueToForm", valueToForm);
      if (invalidDate) {
        setInputValue(null);
        onChange(null);
      } else {
        onChange(valueToForm);
      }
    }
    setVisible(false);
  };

  useEffect(() => {
    const tempValue = moment(value);
    let rawData;
    if (tempValue._isValid) {
      if (mode === "month") {
        rawData = tempValue.add(543, "y").format("DD/MM/YYYY");
      } else {
        rawData = tempValue.add(543, "y").format("MM/YYYY");
      }
      const isInvalidDate = disabledDate(moment(value));
      if (isInvalidDate) {
        return;
      }
      setInputValue(rawData);
    } else setInputValue(null);
  }, [value]);

  return (
    <Style>
      {denyTooltip()}
      <Input
        id={props.name}
        className={`${getCSSDisplayOnly()}`}
        onChange={handleChangeFromText}
        placeholder={mode === "month" ? "DD/MM/YYYY" : "MM/YYYY"}
        value={inputValue}
        onBlur={handleOnBlur}
        onKeyDown={handleKeyDown}
        suffix={
          displayOnly ? (
            <CalendarOutlined />
          ) : (
            <CalendarOutlined
              onClick={() => {
                if (displayOnly || disabled) return true;
                setVisible(true);
              }}
              style={
                disabled
                  ? { cursor: "not-allowed" }
                  : { color: "rgba(0,0,0,.45)", cursor: "pointer" }
              }
            />
          )
        }
        allowClear={allowClear}
        disabled={displayOnly || disabled}
      />
      <Modal
        width="500px"
        fullscreen={false}
        footer={
          <div className="date-submit">
            <Button
              style={{ width: "80px" }}
              type="bmp-fill"
              onClick={handleCloseModal}
            >
              OK
            </Button>
          </div>
        }
        visible={visible}
        onCancel={handleCloseModal}
      >
        <div style={{ width: "450px" }}>
          <Calendar
            key={visible}
            fullscreen={false}
            locale={locale}
            value={value || moment()}
            mode={mode}
            headerRender={({
              value: dateValue,
              type,
              onChange,
              onTypeChange,
            }) => {
              const value = dateValue || moment();
              const start = 0;
              const end = 12;
              const monthOptions = [];

              const current = value.clone();
              const localeData = value.localeData();
              const months = [];
              for (let i = 0; i < 12; i++) {
                current.month(i);
                months.push(localeData.monthsShort(current));
              }

              for (let index = start; index < end; index++) {
                monthOptions.push(
                  <Select.Option className="month-item" key={`${index}`}>
                    {months[index]}
                  </Select.Option>
                );
              }
              const month = value.month();

              const year = value.year();
              const currentYear = moment().year();
              const options = [];
              for (let i = currentYear - 70; i < currentYear + 10; i += 1) {
                options.push(
                  <Select.Option key={i} value={i} className="year-item">
                    {i + 543}
                  </Select.Option>
                );
              }
              return (
                <div style={{ padding: 8 }}>
                  <Typography.Title level={4} style={{ textAlign: "center" }}>
                    {label}
                  </Typography.Title>
                  <Row gutter={8}>
                    <Col span={14} />
                    <Col span={5}>
                      <Select
                        size="small"
                        dropdownMatchSelectWidth={false}
                        className="my-year-select"
                        // value={year || moment().year()}
                        onChange={(newYear) => {
                          const now = value.clone().year(newYear);
                          onChange(now);
                        }}
                        value={year}
                      >
                        {options}
                      </Select>
                    </Col>
                    <Col span={5}>
                      <Select
                        size="small"
                        dropdownMatchSelectWidth={false}
                        value={String(month)}
                        onChange={(selectedMonth) => {
                          const newValue = value.clone();
                          newValue.month(parseInt(selectedMonth, 10));
                          onChange(newValue);
                        }}
                      >
                        {monthOptions}
                      </Select>
                    </Col>
                  </Row>
                </div>
              );
            }}
            disabledDate={disabledDate}
            onSelect={(value) => {
              onChange(value);
              const tempMoment = moment(value);
              let rawData;
              if (mode === "month") {
                rawData = tempMoment.add(543, "y").format("DD/MM/YYYY");
              } else {
                rawData = tempMoment.add(543, "y").format("MM/YYYY");
              }
              const isInvalidDate = disabledDate(value);
              setDateForCheckInvalid(value);
              if (isInvalidDate) {
                return;
              }
              setInputValue(rawData);
            }}
          />
        </div>
      </Modal>
    </Style>
  );
};
const Style = styled.div`
  label: customer-calendar;
  text-align: center;
  .ant-input-affix-wrapper {
    padding: 0 10px;
    border-radius: 8px;
    min-height: 40px;
    .ant-input {
      font-size: 16px;
    }
  }
  .ant-modal-footer {
    text-align: center;
  }

  .input-display-only {
    resize: none;
    border: none;
    background-color: #ffffff;
    color: #0b6623 !important;
    cursor: default !important;
    font-size: 16px;

    .ant-picker-suffix,
    .ant-select-arrow {
      display: none;
    }
    .ant-picker-input > input[disabled],
    .ant-radio-disabled + span,
    .ant-select-disabled,
    .ant-select-selector {
      color: #000000 !important;
      cursor: default !important;
    }
  }
`;
export default DatePickerWithCalendar;
