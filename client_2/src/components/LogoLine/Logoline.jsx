import { useState,useEffect } from "react"
import { useNavigate } from "react-router-dom"
import LogoImg from '../LogoLine/line.png'
import '../LogoLine/logoline.css'
import axios from "axios"


export default function Logoline() {
const recordIntoApi = 1;
const [line, setLine] = useState()

const CustomerCount = () =>{
    const newCount = line+1
    setLine(newCount)
   

}
    
let navigate = useNavigate()
 //--------Click to QRcode---------------
const handleSubmit_logoline = async (e) => {
        await axios.post('http://localhost:8800/api/qrcode/record',
            {   
                "number_of_user_scanners_qr_code": recordIntoApi
            }
        )
    //set coundown 10sec comeback to home page
        setTimeout(() => {
            navigate("/")
                
        }, 10000);
           

    e.preventDefault()
    CustomerCount()
    navigate("/qrcode/record/")
    
}



    return (
        <div className="categories_line" >
            
            <img src={LogoImg} alt="" className="logoimg" onClick={handleSubmit_logoline}/>
            
      </div>
      
    )
    }