import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import "../Master_config/master_config.css";
import Submit from "../Master_config/submit_config.png";
import Close from "../Master_config/close.png";
import Background_MasterConfig from "../Master_config/master configguration page admin.png";
import Detail_MasterConfig from "../Master_config/box.png";
import axios from "axios";

export default function MasterConfig() {
  let navigate = useNavigate();
  const [type, setType] = useState(); //null
  const [duration, setDuration] = useState();
  const [price, setPrice] = useState();

  const [Espresso, setEspresso] = useState("");
  const [Americano, setAmericano] = useState("");
  const [Latte, setLatte] = useState("");
  const [TaiwanTea, setTaiwanTea] = useState("");
  const [ThaiTea, setThaiTea] = useState("");
  const [Soda, setSoda] = useState("");
  const [Cola, setCola] = useState("");
  const [EnergyDrink, setEnergyDrink] = useState("");

  //-----------------------Option menu----------------------------

  const options = [Espresso, Americano, Latte];
  const [selected, setSelected] = useState(options[0]);

  const optionTea = [TaiwanTea, ThaiTea];
  const [selectedTea, setSelectedTea] = useState(options[0]);

  const optionSoftdrink = [Soda, Cola, EnergyDrink];
  const [selectedSoftdrink, setSelectedSoftdrink] = useState(options[0]);

  //--------------------------ID---------------------------------
  const [idEspresso, setidEspresso] = useState();
  const [idAmericano, setidAmericano] = useState();
  const [idLatte, setidLatte] = useState();

  const [idTaiwanTea, setidTaiwanTea] = useState();
  const [idThaiTea, setidThaiTea] = useState();

  const [idSoda, setidSoda] = useState();
  const [idCola, setidCola] = useState();
  const [idEnergyDrink, setidEnergyDrink] = useState();

  //---------------------Duration Coffee---------------------------
  const [durationEspresso, setDurationEspresso] = useState();

  const [durationEspresso1, setDurationEspresso1] = useState();
  const [durationEspresso2, setDurationEspresso2] = useState();
  const [durationEspresso3, setDurationEspresso3] = useState();

  const [durationAmericano, setDurationAmericano] = useState();

  const [durationAmericano1, setDurationAmericano1] = useState();
  const [durationAmericano2, setDurationAmericano2] = useState();
  const [durationAmericano3, setDurationAmericano3] = useState();

  const [durationLatte, setDurationLatte] = useState();

  const [durationLatte1, setDurationLatte1] = useState();
  const [durationLatte2, setDurationLatte2] = useState();
  const [durationLatte3, setDurationLatte3] = useState();

  //---------------------Duration Tea---------------------------
  const [durationTaiwanTea, setDurationTaiwanTea] = useState();

  const [durationTaiwanTea1, setDurationTaiwanTea1] = useState();
  const [durationTaiwanTea2, setDurationTaiwanTea2] = useState();
  const [durationTaiwanTea3, setDurationTaiwanTea3] = useState();

  const [durationThaiTea, setDurationThaiTea] = useState();

  const [durationThaiTea1, setDurationThaiTea1] = useState();
  const [durationThaiTea2, setDurationThaiTea2] = useState();
  const [durationThaiTea3, setDurationThaiTea3] = useState();

  //---------------------Duration Solt drink---------------------------
  const [durationSoda, setDurationSoda] = useState();

  const [durationSoda1, setDurationSoda1] = useState();
  const [durationSoda2, setDurationSoda2] = useState();
  const [durationSoda3, setDurationSoda3] = useState();

  const [durationCola, setDurationCola] = useState();

  const [durationCola1, setDurationCola1] = useState();
  const [durationCola2, setDurationCola2] = useState();
  const [durationCola3, setDurationCola3] = useState();

  const [durationEnergyDrink, setDurationEnergyDrink] = useState();

  const [durationEnergyDrink1, setDurationEnergyDrink1] = useState();
  const [durationEnergyDrink2, setDurationEnergyDrink2] = useState();
  const [durationEnergyDrink3, setDurationEnergyDrink3] = useState();

  //-----------------------Option duration----------------------------

  const optionduration = [durationEspresso, durationAmericano, durationLatte];
  const [selectedduration, setSelectedDuration] = useState(options[0]);

  const optionsdurationEspresso = [
    durationEspresso1,
    durationEspresso2,
    durationEspresso3,
  ];
  const optionsdurationAmericano = [
    durationAmericano1,
    durationAmericano2,
    durationAmericano3,
  ];
  const optionsdurationLatte = [durationLatte1, durationLatte2, durationLatte3];

  //-----------------------Option duration  Tea----------------------------

  const [selecteddurationTea, setSelectedDurationTea] = useState(options[0]);

  const optionsdurationTaiwanTea = [
    durationTaiwanTea1,
    durationTaiwanTea2,
    durationTaiwanTea3,
  ];
  const optionsdurationThaiTea = [
    durationThaiTea1,
    durationThaiTea2,
    durationThaiTea3,
  ];

  //-----------------------Option duration  Solt drink---------------------------

  const [selecteddurationSoltDrink, setSelectedDurationSoltDrink] = useState(
    options[0]
  );

  const optionsdurationSoda = [durationSoda1, durationSoda2, durationSoda3];
  const optionsdurationCola = [durationCola1, durationCola2, durationCola3];
  const optionsduratiEnergyDrink = [
    durationEnergyDrink1,
    durationEnergyDrink2,
    durationEnergyDrink3,
  ];

  //---------------------Price Coffe---------------------------
  const [priceEspresso, setPriceEspresso] = useState();

  const [priceEspresso1, setPriceEspresso1] = useState();
  const [priceEspresso2, setPriceEspresso2] = useState();
  const [priceEspresso3, setPriceEspresso3] = useState();

  const [priceAmericano, setPriceAmericano] = useState();

  const [priceAmericano1, setPriceAmericano1] = useState();
  const [priceAmericano2, setPriceAmericano2] = useState();
  const [priceAmericano3, setPriceAmericano3] = useState();

  const [priceLatte, setPriceLatte] = useState();

  const [priceLatte1, setPriceLatte1] = useState();
  const [priceLatte2, setPriceLatte2] = useState();
  const [priceLatte3, setPriceLatte3] = useState();

  //---------------------Price  Tea----------------------------
  const [priceTaiwanTea, setPriceTaiwanTea] = useState();

  const [priceTaiwanTea1, setPriceTaiwanTea1] = useState();
  const [priceTaiwanTea2, setPriceTaiwanTea2] = useState();
  const [priceTaiwanTea3, setPriceTaiwanTea3] = useState();

  const [priceThaiTea, setPriceThaiTea] = useState();

  const [priceThaiTea1, setPriceThaiTea1] = useState();
  const [priceThaiTea2, setPriceThaiTea2] = useState();
  const [priceThaiTea3, setPriceThaiTea3] = useState();

  //---------------------Price   Solt drink--------------------------
  const [priceSoda, setPriceSoda] = useState();

  const [priceSoda1, setPriceSoda1] = useState();
  const [priceSoda2, setPriceSoda2] = useState();
  const [priceSoda3, setPriceSoda3] = useState();

  const [priceCola, setPriceCola] = useState();

  const [priceCola1, setPriceCola1] = useState();
  const [priceCola2, setPriceCola2] = useState();
  const [priceCola3, setPriceCola3] = useState();

  const [priceEnergyDrink, setPriceEnergyDrink] = useState();

  const [priceEnergyDrink1, setPriceEnergyDrink1] = useState();
  const [priceEnergyDrink2, setPriceEnergyDrink2] = useState();
  const [priceEnergyDrink3, setPriceEnergyDrink3] = useState();

  //-----------------------Option Price----------------------------

  const optionsprice = [priceEspresso, priceAmericano, priceLatte];
  const [selectedprice, setSelectedPrice] = useState(options[0]);

  const optionspriceEspresso = [priceEspresso1, priceEspresso2, priceEspresso3];
  const optionspriceAmericano = [
    priceAmericano1,
    priceAmericano2,
    priceAmericano3,
  ];
  const optionspriceLatte = [priceLatte1, priceLatte2, priceLatte3];

  //-----------------------Option Tea----------------------------

  const [selectedpriceTea, setSelectedPriceTea] = useState(options[0]);

  const optionspriceTaiwanTea = [
    priceTaiwanTea1,
    priceTaiwanTea2,
    priceTaiwanTea3,
  ];

  const optionspriceThaiTea = [priceThaiTea1, priceThaiTea2, priceThaiTea3];

  //-----------------------Option  Solt drink----------------------------

  const [selectedpriceSoltDrink, setSelectedPriceSoltDrink] = useState(
    options[0]
  );

  const optionspriceSoda = [priceSoda1, priceSoda2, priceSoda3];
  const optionspriceCola = [priceCola1, priceCola2, priceCola3];
  const optionspriceEnergyDrink = [
    priceEnergyDrink1,
    priceEnergyDrink2,
    priceEnergyDrink3,
  ];

  //---------------Get Attribute from api-----------------
  useEffect(() => {
    const getAttribute = async () => {
      await axios
        .get("http://localhost:8800/api/master_config/config_beverage")
        .then((res) => {
          console.log(res.data);
          setidEspresso(res.data[0].id);
          setidAmericano(res.data[3].id);
          setidLatte(res.data[6].id);

          setEspresso(res.data[0].config_key);
          setAmericano(res.data[3].config_key);
          setLatte(res.data[6].config_key);

          setDurationEspresso(res.data[0].value_2);
          setDurationAmericano(res.data[3].value_2);
          setDurationLatte(res.data[6].value_2);

          // -------------tea----------------------
          setidTaiwanTea(res.data[9].id);
          setidThaiTea(res.data[10].id);

          setTaiwanTea(res.data[9].config_key);
          setThaiTea(res.data[10].config_key);

          setDurationTaiwanTea(res.data[9].value_2);
          setDurationThaiTea(res.data[10].value_2);

          // -------------solf dink----------------------
          setidSoda(res.data[15].id);
          setidCola(res.data[18].id);
          setidEnergyDrink(res.data[21].id);

          setSoda(res.data[15].config_key);
          setCola(res.data[18].config_key);
          setEnergyDrink(res.data[21].config_key);

          setDurationSoda(res.data[15].value_2);
          setDurationCola(res.data[18].value_2);
          setDurationEnergyDrink(res.data[21].value_2);

          // ---------------coffe---price--------------------------
          setPriceEspresso(res.data[0].value_1);

          setPriceEspresso1(res.data[0].value_1);
          setPriceEspresso2(res.data[1].value_1);
          setPriceEspresso3(res.data[2].value_1);

          setPriceAmericano(res.data[3].value_1);

          setPriceAmericano1(res.data[3].value_1);
          setPriceAmericano2(res.data[4].value_1);
          setPriceAmericano3(res.data[5].value_1);

          setPriceLatte(res.data[6].value_1);

          setPriceLatte1(res.data[6].value_1);
          setPriceLatte2(res.data[7].value_1);
          setPriceLatte3(res.data[8].value_1);

          // ------------coffee----duration--------------------------
          setDurationEspresso(res.data[0].value_2);

          setDurationEspresso1(res.data[0].value_2);
          setDurationEspresso2(res.data[1].value_2);
          setDurationEspresso3(res.data[2].value_2);

          setDurationAmericano(res.data[3].value_2);

          setDurationAmericano1(res.data[3].value_2);
          setDurationAmericano2(res.data[4].value_2);
          setDurationAmericano3(res.data[5].value_2);

          setDurationLatte(res.data[6].value_2);

          setDurationLatte1(res.data[6].value_2);
          setDurationLatte2(res.data[7].value_2);
          setDurationLatte3(res.data[8].value_2);

          // ---------------Tea---price--------------------------
          setPriceTaiwanTea(res.data[9].value_1);

          setPriceTaiwanTea1(res.data[9].value_1);
          setPriceTaiwanTea2(res.data[13].value_1);
          setPriceTaiwanTea3(res.data[14].value_1);

          setPriceThaiTea(res.data[10].value_1);

          setPriceThaiTea1(res.data[10].value_1);
          setPriceThaiTea2(res.data[11].value_1);
          setPriceThaiTea3(res.data[12].value_1);

          // ------------Tea----duration--------------------------
          setDurationTaiwanTea(res.data[9].value_2);

          setDurationTaiwanTea1(res.data[9].value_2);
          setDurationTaiwanTea2(res.data[13].value_2);
          setDurationTaiwanTea3(res.data[14].value_2);

          setDurationThaiTea(res.data[10].value_2);

          setDurationThaiTea1(res.data[10].value_2);
          setDurationThaiTea2(res.data[11].value_2);
          setDurationThaiTea3(res.data[12].value_2);

          // ---------------Solt drink---price--------------------------
          setPriceSoda(res.data[15].value_1);

          setPriceSoda1(res.data[15].value_1);
          setPriceSoda2(res.data[16].value_1);
          setPriceSoda3(res.data[17].value_1);

          setPriceCola(res.data[18].value_1);

          setPriceCola1(res.data[18].value_1);
          setPriceCola2(res.data[19].value_1);
          setPriceCola3(res.data[20].value_1);

          setPriceEnergyDrink(res.data[21].value_1);

          setPriceEnergyDrink1(res.data[21].value_1);
          setPriceEnergyDrink2(res.data[22].value_1);
          setPriceEnergyDrink3(res.data[23].value_1);

          // ------------Solt drink----duration--------------------------
          setDurationSoda(res.data[15].value_2);

          setDurationSoda1(res.data[15].value_2);
          setDurationSoda2(res.data[16].value_2);
          setDurationSoda3(res.data[17].value_2);

          setDurationCola(res.data[18].value_2);

          setDurationCola1(res.data[18].value_2);
          setDurationCola2(res.data[19].value_2);
          setDurationCola3(res.data[20].value_2);

          setDurationEnergyDrink(res.data[21].value_2);

          setDurationEnergyDrink1(res.data[21].value_2);
          setDurationEnergyDrink2(res.data[22].value_2);
          setDurationEnergyDrink3(res.data[23].value_2);
        });
    };
    getAttribute();
  }, []);

  // --------Click OK back to home-----------------
  const handleSubmit = async (e) => {
    e.preventDefault();

    await axios.post(
      `http://localhost:8800/api/master_config/update_beverage`,
      {
        type_coffee: selected,
        price_coffee: selectedprice,
        duration_coffee: selectedduration,
        type_tea: selectedTea,
        price_tea: selectedpriceTea,
        duration_tea: selecteddurationTea,
        type_soft_drink: selectedSoftdrink,
        price_soft_drink: selectedpriceSoltDrink,
        duration_soft_drink: selecteddurationSoltDrink,
      }
    );
    navigate("/");
  };

  const handleClose = (e) => {
    e.preventDefault();
    navigate("/");
  };

  return (
    <div className="background-master-config">
      <div className="background_masterconfig">
        <img src={Detail_MasterConfig} alt="" className="img_masterconfig" />

        <div className="detail_product">
          <div className="mc_coffee">
            {/* dropdown ต้องดึงมาจาก database  example.. coffe */}
            <select
              className="dropdown_type"
              value={selected}
              onChange={(e) => setSelected(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select price
              </option>
              {options.map((value) => (
                <option value={value} key={value}>
                  {value}
                </option>
              ))}
            </select>

            <select
              className="dropdown_price"
              value={selectedprice}
              onChange={(e) => setSelectedPrice(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select price
              </option>
              {selected == "Espresso"
                ? optionspriceEspresso.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selected == "Americano"
                ? optionspriceAmericano.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selected == "Latte"
                ? optionspriceLatte.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>

            <select
              className="dropdown_duration"
              value={selectedduration}
              onChange={(e) => setSelectedDuration(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select duration
              </option>
              {selected == "Espresso"
                ? optionsdurationEspresso.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selected == "Americano"
                ? optionsdurationAmericano.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selected == "Latte"
                ? optionsdurationLatte.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>
          </div>

          {/* ---------------------------------------------Tea---------------------------------------------------- */}
          <div className="mc_price">
            <select
              className="dropdown_type"
              value={selectedTea}
              onChange={(e) => setSelectedTea(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select type
              </option>
              {optionTea.map((value) => (
                <option value={value} key={value}>
                  {value}
                </option>
              ))}
            </select>

            <select
              className="dropdown_price"
              value={selectedpriceTea}
              onChange={(e) => setSelectedPriceTea(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select price
              </option>
              {selectedTea == "Taiwan tea"
                ? optionspriceTaiwanTea.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedTea == "Thai tea"
                ? optionspriceThaiTea.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>

            <select
              className="dropdown_duration"
              value={selecteddurationTea}
              onChange={(e) => setSelectedDurationTea(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select duration
              </option>
              {selectedTea == "Taiwan tea"
                ? optionsdurationTaiwanTea.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedTea == "Thai tea"
                ? optionsdurationThaiTea.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>
          </div>

          <div className="mc_duration">
            <select
              className="dropdown_type"
              value={selectedSoftdrink}
              onChange={(e) => setSelectedSoftdrink(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select type
              </option>
              {optionSoftdrink.map((value) => (
                <option value={value} key={value}>
                  {value}
                </option>
              ))}
            </select>

            <select
              className="dropdown_price"
              value={selectedpriceSoltDrink}
              onChange={(e) => setSelectedPriceSoltDrink(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select price
              </option>
              {selectedSoftdrink == "Soda"
                ? optionspriceSoda.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedSoftdrink == "Cola"
                ? optionspriceCola.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedSoftdrink == "Energy drink"
                ? optionspriceEnergyDrink.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>

            <select
              className="dropdown_duration"
              value={selecteddurationSoltDrink}
              onChange={(e) => setSelectedDurationSoltDrink(e.target.value)}
            >
              <option value="" disabled selected hidden>
                select duration
              </option>
              {selectedSoftdrink == "Soda"
                ? optionsdurationSoda.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedSoftdrink == "Cola"
                ? optionsdurationCola.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : selectedSoftdrink == "Energy drink"
                ? optionsduratiEnergyDrink.map((value) => (
                    <option value={value} key={value}>
                      {value}
                    </option>
                  ))
                : ""}
            </select>
          </div>

          <div className="button_group">
            <img
              src={Submit}
              alt=""
              className="button_submit"
              onClick={handleSubmit}
            />
            <img
              src={Close}
              alt=""
              className="button_close"
              onClick={handleClose}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
