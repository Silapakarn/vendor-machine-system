import { useState } from "react";
import { useNavigate } from "react-router-dom";
import ButtonLogin from "../Login/RegisterLogin Button.png";
import "../Login/login.css";

export default function Login() {
  let navigate = useNavigate();
  const [Login, setLogin] = useState();

  const CustomerCount = () => {
    navigate("/LoginMasterConfig");
  };

  return (
    <div className="Button_Login">
      {{ Login } && (
        <img
          src={ButtonLogin}
          alt=""
          className="loginimg"
          onClick={CustomerCount}
        />
      )}
    </div>
  );
}
