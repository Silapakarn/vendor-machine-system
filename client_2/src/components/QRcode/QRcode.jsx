import '../QRcode/qrcode.css'
import Categories from '../Categories/Categories'
import { useNavigate } from "react-router-dom"
import {useState} from 'react'
import QR from '../QRcode/QR.png'
import OK from '../QRcode/button_qr.png'




export default function QRCode() {

    let navigate = useNavigate()

    // const [qr_code, setQR_code] = useState('')

    // --------Click OK back to home----------------- 
    const handleSubmit_OK = (e) => {
        e.preventDefault()
        navigate("/")
    }

    
    return (
    <div className="qr_line">

      <div className="div_qr">
        <img src={QR} alt="" className="photo_QR"/>
        <img src={OK} alt="" className="button_OK" onClick={handleSubmit_OK}/>
        
      </div>
  
    </div>
)}
