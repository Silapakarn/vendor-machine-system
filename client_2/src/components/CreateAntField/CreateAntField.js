import React from "react";
import { FastField } from "formik";
import styled from "@emotion/styled";
import Cleave from "cleave.js/react";
import isEmpty from "lodash/isEmpty";
import get from "lodash/get";
import DatePicker from "antd/lib/date-picker";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import TimePicker from "antd/lib/time-picker";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Checkbox from "antd/lib/checkbox";
import { uniqueId } from "lodash";

import TableTransfer from "../TableTransfer/TableTransfer";
import AutoComplete from "../Autocomplete/Autocomplete";
import UploadComponent from "../UploadComponent/UploadComponent";
import DatePickerWithCalendar from "../DatePickerWithCalendar/DatePickerWithCalendar";

const FormItem = Form.Item;
const { TextArea } = Input;
const { RangePicker } = DatePicker;

const CreateAntField =
  (AntComponent, typeInitialValue, type) =>
  ({
    hasFeedback,
    errors,
    touched,
    name,
    submitCount,
    setFieldValue: setFieldValueFromParents,
    setFieldTouched,
    formOptions,
    upper,
    showSearch = true,
    layout = {},
    prefixIcon,
    placeholder = "",
    onBlur = false,
    onClick = false,
    onFocus = () => {},
    required: inputRequired = false,
    addonAfter = null,
    value,
    moreField = false,
    fastField = false,
    format = "DD/MM/YYYY",
    displayOnly = false,
    fetchDataSuccess = false,
    ...props
  }) => {
    const { label } = props;
    const componentTouched = get(touched, name, false);
    const submitted = submitCount > 0;
    const hasError = get(errors, name);
    const submittedError = hasError && submitted;
    const touchedError = hasError && componentTouched;
    const required = displayOnly ? false : inputRequired;

    const getCSSDisplayOnly = () => {
      if (displayOnly) return "input-display-only";
      return "";
    };

    const getCSSByType = () => {
      if (!value && value !== 0) return "";
      if (type === "number") return "input-number";
      return "";
    };

    if (fastField)
      return (
        <FastField name={name}>
          {({ field, form: { setFieldValue }, meta }) => {
            const whenSetValue = {
              date: "blur",
              text: "blur",
              search: "blur",
              password: "change",
              "text-area": "blur",
              transfer: "blur",
              "check-box": "change",
              radio: "change",
              select: "change",
              complete: "blur",
            };

            const onInputChange = ({
              target: { value: newValue, rawValue },
            }) => {
              if (moreField) {
                if (whenSetValue[type] === "change")
                  setFieldValue(
                    name,
                    upper ? newValue.toUpperCase() : newValue
                  );
              } else
                setFieldValueFromParents(
                  name,
                  upper ? newValue.toUpperCase() : newValue
                );
            };
            const onChange = (newValue) => {
              if (moreField) {
                if (whenSetValue[type] === "change")
                  setFieldValueFromParents(
                    name,
                    upper ? newValue.toUpperCase() : newValue
                  );
              } else
                setFieldValueFromParents(
                  name,
                  upper ? newValue.toUpperCase() : newValue
                );
            };
            const handleOnBlur = ({ target: { value: newValue } }) => {
              if (whenSetValue[type] === "change") return null;

              if (moreField) {
                if (!componentTouched) {
                  setFieldTouched(name, true);
                }
              }
              if (value === newValue) return null;
              setFieldValueFromParents(name, newValue);
            };
            const handleFocus = () => {
              if (!moreField) {
                if (!componentTouched) {
                  setFieldTouched(name, true);
                }
              }
            };

            const checkFormOptions = () => {
              const getPlaceholder = () => {
                if (displayOnly) return " ";
                if (placeholder) return placeholder;
                if (type === "select") return "-Select-";
                if (type === "date") return format;
                return label;
              };
              if (!isEmpty(formOptions)) {
                if (!addonAfter) {
                  return (
                    <Cleave
                      name={name}
                      value={value}
                      options={formOptions || {}}
                      autoComplete="off"
                      className="ant-input full-width"
                      id={`${props.name ? props.name : uniqueId()}${
                        props.label ? props.label : uniqueId()
                      }`}
                      {...props}
                      onBlur={handleOnBlur}
                      onFocus={handleFocus}
                      onChange={(e) => onChange(e.target.rawValue)}
                      placeholder={getPlaceholder()}
                    />
                  );
                }

                return (
                  <span className="ant-input-wrapper ant-input-group">
                    <Cleave
                      name={name}
                      value={value}
                      options={formOptions || {}}
                      autoComplete="off"
                      className="ant-input full-width"
                      id={`${props.name ? props.name : uniqueId()}${
                        props.label ? props.label : uniqueId()
                      }`}
                      {...props}
                      onBlur={handleOnBlur}
                      onFocus={handleFocus}
                      onChange={(e) => onChange(e.target.rawValue)}
                      placeholder={getPlaceholder()}
                    />
                    {addonAfter && (
                      <span className="ant-input-group-addon">
                        {addonAfter}{" "}
                      </span>
                    )}
                  </span>
                );
              }

              let initialvalue = {};
              if (typeInitialValue === "defaultValue")
                initialvalue = { defaultValue: value };
              else if (type === "password") initialvalue = {};
              else initialvalue = { value };
              return (
                <AntComponent
                  name={name}
                  showSearch={showSearch}
                  size="large"
                  addonAfter={addonAfter}
                  className="full-width"
                  autoComplete="off"
                  id={name}
                  format={format}
                  onBlur={handleOnBlur}
                  onFocus={handleFocus}
                  placeholder={getPlaceholder()}
                  onChange={(e) =>
                    get(e, "target", false) ? onInputChange(e) : onChange(e)
                  }
                  filterOption={
                    showSearch
                      ? (input, option) =>
                          option.label
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                      : null
                  }
                  {...initialvalue}
                  {...props}
                />
              );
            };
            return (
              <CreateInput className="field-container">
                <FormItem
                  colon={false}
                  key={
                    moreField
                      ? `${!value}${name}${props.disabled}`
                      : props.disabled
                      ? !value
                      : `${name}${props.disabled}${fetchDataSuccess}`
                  }
                  {...layout}
                  {...props}
                  label={
                    label ? (
                      <>
                        {label}
                        {required ? (
                          <span style={{ color: "red", margin: "0 0 0 5px" }}>
                            *
                          </span>
                        ) : null}
                      </>
                    ) : (
                      ""
                    )
                  }
                  hasFeedback={
                    (hasFeedback && submitted) ||
                    (hasFeedback && componentTouched)
                      ? true
                      : false
                  }
                  help={
                    submittedError || touchedError
                      ? hasError === " "
                        ? false
                        : hasError
                      : false
                  }
                  validateStatus={
                    submittedError || touchedError ? "error" : false
                  }
                >
                  {prefixIcon ? (
                    <>
                      {prefixIcon}
                      {checkFormOptions()}
                    </>
                  ) : (
                    checkFormOptions()
                  )}
                </FormItem>
              </CreateInput>
            );
          }}
        </FastField>
      );
    else {
      const onInputChange = ({ target: { value: newValue, rawValue } }) =>
        setFieldValueFromParents(
          name,
          upper ? newValue.toUpperCase() : newValue
        );

      const onChange = (newValue) =>
        setFieldValueFromParents(
          name,
          upper ? newValue.toUpperCase() : newValue
        );

      const handleOnBlur = () => {
        if (onBlur) onBlur();
        setFieldTouched(name, true);
      };

      const handleOnClick = () => {
        if (onClick) onClick({ name });
      };
      const checkFormOptions = () => {
        const { disabled, ...newProps } = props;

        const getPlaceholder = () => {
          if (displayOnly) return " ";
          if (placeholder) return placeholder;
          if (type === "select") return "-Select-";
          if (type === "date") return format;
          return label;
        };

        if (!isEmpty(formOptions)) {
          if (!addonAfter)
            return (
              <>
                <Cleave
                  name={name}
                  options={formOptions || {}}
                  autoComplete="off"
                  className={`ant-input full-width input-cleave ${getCSSDisplayOnly()}  ${getCSSByType()}`}
                  id={`${props.name ? props.name : uniqueId()}${
                    props.label ? props.label : uniqueId()
                  }`}
                  {...props}
                  onBlur={handleOnBlur}
                  onChange={(e) => onChange(e.target.rawValue)}
                  placeholder={getPlaceholder()}
                  disabled={displayOnly || disabled}
                  value={value}
                />
              </>
            );
          return (
            <span className="ant-input-wrapper ant-input-group">
              <Cleave
                name={name}
                options={formOptions || {}}
                autoComplete="off"
                className={`ant-input full-width input-cleave ${getCSSDisplayOnly()} ${getCSSByType()}`}
                id={`${props.name ? props.name : uniqueId()}${
                  props.label ? props.label : uniqueId()
                }`}
                {...props}
                onBlur={handleOnBlur}
                onChange={(e) => onChange(e.target.rawValue)}
                placeholder={getPlaceholder()}
                disabled={displayOnly || disabled}
                value={value}
              />
              {addonAfter && (
                <span className="ant-input-group-addon">{addonAfter} </span>
              )}
            </span>
          );
        }
        return (
          <div
            className={
              addonAfter && "ant-input-wrapper ant-input-group addon-select"
            }
          >
            <AntComponent
              name={name}
              showSearch={showSearch}
              addonAfter={addonAfter}
              size="large"
              className={`full-width ${getCSSDisplayOnly()} ${getCSSByType()}`}
              autoComplete="off"
              id={name}
              format={format}
              onBlur={handleOnBlur}
              placeholder={getPlaceholder()}
              onChange={(e) =>
                get(e, "target", false) ? onInputChange(e) : onChange(e)
              }
              filterOption={
                showSearch
                  ? (input, option) =>
                      option.label.toLowerCase().indexOf(input.toLowerCase()) >=
                      0
                  : null
              }
              value={value}
              allowClear={displayOnly ? false : type === "select" && !required}
              disabled={displayOnly || disabled}
              setFieldValue={setFieldValueFromParents}
              displayOnly={displayOnly}
              {...newProps}
            />
            {addonAfter && (
              <span className="ant-input-group-addon">{addonAfter}</span>
            )}
          </div>
        );
      };
      return (
        <CreateInput className="field-container">
          <FormItem
            colon={false}
            {...layout}
            {...props}
            label={
              label ? (
                <>
                  {label}{" "}
                  {required ? (
                    <span style={{ color: "red", margin: "0 0 0 5px" }}>*</span>
                  ) : null}
                </>
              ) : (
                ""
              )
            }
            hasFeedback={
              (hasFeedback && submitted) || (hasFeedback && componentTouched)
                ? true
                : false
            }
            help={
              submittedError || touchedError
                ? hasError === " "
                  ? false
                  : hasError
                : false
            }
            validateStatus={submittedError || touchedError ? "error" : false}
            onClick={handleOnClick}
          >
            {prefixIcon ? (
              <>
                {prefixIcon}
                {checkFormOptions()}
              </>
            ) : (
              checkFormOptions()
            )}
          </FormItem>
        </CreateInput>
      );
    }
  };
const CreateInput = styled.div`
  .full-width {
    width: 100%;
  }
  .input-has-prefix {
    display: flex;
    border-bottom: solid 1px #000000;
  }

  input:focus + .input-has-prefix {
    border-bottom: solid 1px red !important;
    padding: none;
  }

  .ant-input:focus {
    box-shadow: 0 0 0 2px rgb(81 198 121 / 34%);
  }

  .ant-form-item {
    margin-bottom: 16px;
  }

  .ant-form-item-label {
    label {
      min-height: 44px;
      font-size: 16px;
    }
  }

  .ant-input,
  .ant-picker,
  .ant-select:not(.ant-select-customize-input) .ant-select-selector {
    border-radius: 8px;
    min-height: 45px;
  }

  .addon-select
    .ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    border-radius: 8px 0 0 8px;
  }

  .input-number {
    text-align: right;
  }

  .ant-input-search
    > .ant-input-group
    > .ant-input-group-addon:last-child
    .ant-input-search-button {
    padding: 4px;
    height: 43px;
    border-radius: 0 8px 8px 0;
  }
  .ant-input-search-button {
    height: 32px;
  }

  .add-on-display-only {
    display: flex;
    height: 40px;
    align-items: center;
    padding: 0 11px;
    font-size: 16px;
  }
  .input-display-only {
    resize: none;
    border: none;
    background-color: #ffffff;
    color: #0b6623 !important;
    cursor: default !important;
    font-size: 16px;
    border-radius: 8px;

    .ant-picker-suffix,
    .ant-select-arrow {
      display: none;
    }
    .ant-picker-input > input[disabled],
    .ant-radio-disabled + span,
    .ant-select-disabled,
    .ant-select-selector,
    .ant-select-selector > .ant-select-selection-item {
      color: #0b6623 !important;
      cursor: default !important;
    }
  }
  .ant-input-group-addon:last-child {
    border-radius: 0 8px 8px 0;
  }
  .ant-input-lg {
    font-size: 16px;
  }
  .ant-checkbox-group,
  .ant-radio-group {
    margin: 10px 0 0;
  }

  .ant-select-single.ant-select-lg:not(.ant-select-customize-input) {
    .ant-select-selection-item,
    .ant-select-selection-placeholder {
      line-height: 42px;
    }
    .ant-select-selection-search-input {
      height: 42px;
    }
  }
`;
const AntDatePicker = CreateAntField(DatePickerWithCalendar, "", "date");
const AntDatePickerWithCalendar = CreateAntField(
  DatePickerWithCalendar,
  "",
  "date"
);
const AntRangePicker = CreateAntField(RangePicker, "", "date");
const TextInput = CreateAntField(Input, "defaultValue", "text");
const InputNumber = CreateAntField(Input, "defaultValue", "number");
const Search = CreateAntField(Input.Search, "", "search");
const PasswordInput = CreateAntField(Input.Password, "", "password");
const AntTimePicker = CreateAntField(TimePicker, "", "date");
const AntTextArea = CreateAntField(TextArea, "defaultValue", "text-area");
const AntTableTransfer = CreateAntField(TableTransfer, "", "transfer");

const AntCheckbox = CreateAntField(Checkbox.Group, "", "check-box");
const AntRadio = CreateAntField(Radio.Group, "", "radio");
const AntSelect = CreateAntField(Select, "", "select");
const AntAutocomplete = CreateAntField(AutoComplete, "", "auto-complete");
const AntUpload = UploadComponent;

export {
  AntUpload,
  AntDatePicker,
  AntDatePickerWithCalendar,
  AntRangePicker,
  TextInput,
  Search,
  PasswordInput,
  AntTimePicker,
  AntTextArea,
  AntTableTransfer,
  AntCheckbox,
  AntRadio,
  AntSelect,
  AntAutocomplete,
  InputNumber,
};

export default CreateAntField(Input);
