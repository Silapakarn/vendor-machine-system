import Styled from '@emotion/styled'
import { Upload, Button, Form } from 'antd/lib'
import { UploadOutlined } from '@ant-design/icons'
import get from 'lodash/get'
import uniqueId from 'lodash/uniqueId'

const FormItem = Form.Item
const AntUpload = ({
  name = '',
  setFieldValue = false,
  value = [],
  required = false,
  children = false,
  multiFile = false,
  showImg = false,
  submitCount,
  errors,
  componentTouched,
  listType = '',
  ...props
}) => {
  const submitted = submitCount > 0
  const hasError = get(errors, name)
  const submittedError = hasError && submitted
  const touchedError = hasError && componentTouched

  const renderChildren = () => {
    if (!children) return <Button icon={<UploadOutlined />}>Upload</Button>
    return children
  }
  return (
    <StyledComponent className='field-container'>
      <FormItem
        help={submittedError || touchedError ? hasError : false}
        validateStatus={submittedError || touchedError ? 'error' : false}
      >
        <Upload
          id={`${props.name ? props.name : uniqueId()}${props.label ? props.label : uniqueId()}`}
          onChange={({ file, fileList }) => {
            const setDone = fileList.map((fileItem) => ({ ...fileItem, status: 'done' }))
            if (multiFile) setFieldValue(name, setDone)
            else {
              if (fileList.length) setFieldValue(name, [{ ...file, status: 'done' }])
              else setFieldValue(name, [])
            }
          }}
          fileList={value}
          listType={listType}
          {...props}
        >
          {renderChildren()}
        </Upload>
      </FormItem>
    </StyledComponent>
  )
}

const StyledComponent = Styled.div`
label: ant-upload;
  .full-width {
    width: 100%;
  }
`
export default AntUpload
