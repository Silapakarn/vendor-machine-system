import React from "react";
import styled from '@emotion/styled'

export const GrennButton = ({ onClick, GrennButtonName }) => {

    return(
        <Style>
            <button class="button-41" role="button" onClick={onClick}> {GrennButtonName}</button>
        </Style>
    )
}

const Style = styled.div`
.button-41 {
  background-color: initial;
  background-image: linear-gradient(-180deg, #55e280, #55e280);
  border-radius: 15px;
  box-shadow: rgba(0, 0, 0, 0.1) 0 2px 4px;
  color: #ffffff;
  cursor: pointer;
  display: inline-block;
  font-family: Inter, -apple-system, system-ui, Roboto, "Helvetica Neue", Arial,
    sans-serif;
  height: 44px;
  line-height: 44px;
  outline: 0;
  overflow: hidden;
  padding: 0 40px;
  pointer-events: auto;
  position: relative;
  text-align: center;
  touch-action: manipulation;
  user-select: none;
  -webkit-user-select: none;
  vertical-align: top;
  white-space: nowrap;
  width: 72%;
  z-index: 9;
  border: 0;
}

.button-41:hover {
  background: #3cc365;
}
`

export default GrennButton
