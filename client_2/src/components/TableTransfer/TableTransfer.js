import styled from '@emotion/styled'
import { useState } from 'react'
import { Transfer, Table, Button } from 'antd/lib'
import difference from 'lodash/difference'
import { sortableContainer, sortableElement, sortableHandle } from 'react-sortable-hoc'
import { DragOutlined } from '@ant-design/icons'
// import arrayMove from 'array-move'

const DragHandle = sortableHandle(() => (
    <div className='drag-icon'>
      <DragOutlined />
    </div>
  ))

  const SortableItem = sortableElement((props) => <tr {...props} />)
  const SortableContainer = sortableContainer((props) => <tbody {...props} />)
  
  const TableTransfer = ({
    leftColumns: tempLeftColumns,
    rightColumns: tempRightColumns,
    canOrder = false,
    name,
    setFieldValue,
    displayOnly: displayOnlyProps = false,
    ...restProps
  }) => {
    const leftColumns = tempLeftColumns.map((column) => ({
      ...column,
      title: <div className='tr-center'> {column.title || ''}</div>,
    }))
  
    const rightColumns = tempRightColumns.map((column) => ({
      ...column,
      title: <div className='tr-center'>{column.title || ''}</div>,
    }))
  
    const columnNo = [
      ...rightColumns,
      {
        title: 'Priority',
        dataIndex: 'order',
        width: 30,
        render: (_, __, index) => <div className='tr-center'> {index + 1}</div>,
      },
    ]
    const [columnsDrag, setColumnDrag] = useState(columnNo)
    const [isOrder, setIsOrder] = useState(false)
    const [displayOnly] = useState(displayOnlyProps)
  
    return (
      <TransferStyle {...restProps} showSelectAll={false}>
        {({
          direction,
          filteredItems,
          onItemSelectAll,
          onItemSelect,
          selectedKeys: listSelectedKeys,
          disabled: listDisabled,
  
          ...otherProps
        }) => {
          const columns = direction === 'left' ? leftColumns : rightColumns
  
          const rowSelection = {
            getCheckboxProps: (item) => ({ disabled: listDisabled || item.disabled }),
            onSelectAll(selected, selectedRows) {
              const treeSelectedKeys = selectedRows
                .filter((item) => !item.disabled)
                .map(({ key }) => key)
              const diffKeys = selected
                ? difference(treeSelectedKeys, listSelectedKeys)
                : difference(listSelectedKeys, treeSelectedKeys)
              onItemSelectAll(diffKeys, selected)
            },
            onSelect({ key }, selected) {
              onItemSelect(key, selected)
            },
            selectedRowKeys: listSelectedKeys,
          }
  
          const onRow = ({ key, disabled: itemDisabled }) => ({
            onClick: () => {
              if (itemDisabled || listDisabled) return
              onItemSelect(key, !listSelectedKeys.includes(key))
            },
          })
  
          const handleDragBtn = () => {
            setColumnDrag([
              {
                title: '',
                dataIndex: 'sort',
                width: 45,
                className: 'drag-visible',
                render: (_, __, index) => (
                  <div className='tr-center'>
                    <DragHandle />
                  </div>
                ),
              },
              ...columnNo,
            ])
            setIsOrder(true)
          }
  
          const handleMove = () => {
            setColumnDrag(columnNo)
            setIsOrder(false)
          }
  
          const onSortEnd = ({ oldIndex, newIndex }) => {
            if (oldIndex !== newIndex) {
              {/* const newData = arrayMove([].concat(filteredItems), oldIndex, newIndex).filter(
                (el) => !!el
              ) */}
              {/* const listData = newData.map(({ key }) => `${key}`) */}
              {/* setFieldValue(name, listData) */}
            }
          }
  
          const draggableContainer = (props) => (
            <SortableContainer
              useDragHandle
              disableAutoscroll
              helperClass='row-dragging'
              onSortEnd={onSortEnd}
              {...props}
            />
          )
  
          const draggableBodyRow = ({ className, style, ...restProps }) => {
            const index = filteredItems.findIndex((x) => x.index === restProps['data-row-key'])
            return <SortableItem index={index} {...restProps} />
          }
  
          if (direction === 'left') {
            return (
              <Table
                rowSelection={rowSelection}
                columns={columns}
                dataSource={filteredItems}
                size='small'
                style={{ pointerEvents: listDisabled ? 'none' : null }}
                onRow={onRow}
                bordered
              />
            )
          }
  
          if (canOrder) {
            return (
              <>
                <Table
                  rowSelection={!isOrder && rowSelection}
                  rowKey={isOrder ? 'index' : 'key'}
                  dataSource={filteredItems}
                  columns={columnsDrag}
                  onRow={!isOrder && onRow}
                  size='small'
                  components={{
                    body: {
                      wrapper: draggableContainer,
                      row: draggableBodyRow,
                    },
                  }}
                  bordered
                />
                {!displayOnly && (
                  <div className='tr-right footer'>
                    <Button className='btn-action' onClick={handleMove} disabled={!isOrder}>
                      Move Data
                    </Button>
                    <Button
                      className='btn-action'
                      type='bmp'
                      onClick={handleDragBtn}
                      disabled={isOrder}
                    >
                      Edit Priority
                    </Button>
                  </div>
                )}
              </>
            )
          }
          return (
            <>
              <Table
                rowSelection={rowSelection}
                columns={columns}
                dataSource={filteredItems}
                size='small'
                style={{ pointerEvents: listDisabled ? 'none' : null }}
                onRow={onRow}
                bordered
              />
            </>
          )
        }}
      </TransferStyle>
    )
  }
  
  const TransferStyle = styled(Transfer)`
    label: custom-transfer;
    .btn-action {
      border-radius: 8px !important;
      margin-left: 16px;
      width: 128px;
    }
    .footer {
      padding: 16px;
    }
    .td-drag {
      display: flex !important;
      align-items: center;
    }
    .tr-center {
      padding: 0 8px;
    }
    .ant-table-selection-column {
      font-size: 20px !important;
      width: 53px;
    }
    th {
      height: 48px !important;
    }
  `
  
  export default TableTransfer
  