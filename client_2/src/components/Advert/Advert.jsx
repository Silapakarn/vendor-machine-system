import "./advert.css";
import Categories from "../Categories/Categories";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import PhotoAdvert from "../Advert/photoadvert.png";
import quit from "./button_out.png";

export default function Avert() {
  let navigate = useNavigate();

  const [model, setModel] = useState(true);
  const [quitAdvert, setQuitAdvert] = useState("");

  // --------close advert-----------------

  const handleClose = () => {
    setModel(false);
  };

 
  return (
    <div>
      {model && (
        <div className="div_advert">
          <img src={PhotoAdvert} alt="" className="photo_advert" />
          <img
            src={quit}
            alt=""
            className="button_quit"
            onClick={handleClose}
          />
        </div>
      )}
    </div>
  );
}
