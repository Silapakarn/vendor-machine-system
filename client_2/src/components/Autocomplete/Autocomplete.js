import AutoComplete from 'antd/lib/auto-complete'

const Component = () => {
  return (
    <AutoComplete
      filterOption={(inputValue, option) =>
        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
      }
    />
  )
}

export default Component