import React from 'react'
import Navbar from '../Navbar/Navbar.jsx'
import Categories from '../Categories/Categories.jsx'
import Logoline from '../LogoLine/Logoline.jsx'
import Avert from '../Advert/Advert.jsx'
import Login from '../Login/Login.jsx'
import '../Home/home.css'
import '../Home/home page.png'



export default function Home() {
  return (
    <div className='background' >
        <Avert/>
        <Categories />
        <Logoline/>
        <Login/>
    </div>
  )
}
