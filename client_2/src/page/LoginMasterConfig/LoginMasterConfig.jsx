import { useState, useEffect } from "react";
import axios from "axios";
import { useForm, FormProvider } from "react-hook-form";
import "../LoginMasterConfig/loginMasterConfig.css";
import loginPg from "./img/vecteezy_smartphone-app-layout_ 1.png";
import { Input } from "../../components/Input/Input";
import { GrennButton } from "../../components/GreenButton/GrennButton.jsx";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function LoginMasterConfig() {
  const methods = useForm();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  let navigate = useNavigate();

  const onSubmit = async (e) => {
    e.preventDefault();
    if (username.length === 0 || password.length === 0) {
      setError(true);
    } else {
      const modelId = {
          "username": username,
          "password": password,
      }
      console.log(modelId);
      try {
        const result = await axios.post("http://localhost:8800/api/auth/token", modelId);
        localStorage.setItem("token", result.data.token);
        if (result.data.status == "ok") {
            toast.success("Login Successfull ! ",{
              position: "top-center",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            })
            setTimeout(() => { navigate("/update/categories") }, 1500);
        } else {
          alert("login failed");
        }
      } catch (error) {
        toast.error("Login Failed. ",{
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        })
        console.log("Error:", error);
      }
    }
  };

  useEffect(() => {
  }, []);

  return (
    <FormProvider {...methods}>
      <form noValidate className="container">
      <ToastContainer />
        <div className="login_page_admin">
          <div className="form">
            <div className="login_form">
              <div className="login_container">
                <div className="zone_left">
                  <div className="zone_left_container">
                    <h3 className="WELCOME">WELCOME</h3>
                    <div>this account use for admin or users</div>
                    <div>associated with the store only</div>
                    <div class="input_container">
                      <div class="input-wrapper">
                        <input
                          className="username"
                          type="text"
                          id="username"
                          required
                          placeholder="Username"
                          onChange={(e) => setUsername(e.target.value)}
                        />
                        {error && username.length <= 0 ? (
                          <div style={{ color: "#F53636", marginTop: "0.3rem" }}>
                            Please enter your username !
                          </div>
                        ) : (
                          ""
                        )}
                        <label for="user">user name</label>
                      </div>
                      <div class="input-wrapper">
                        <input
                          className="password"
                          type="password"
                          required
                          placeholder="Password"
                          onChange={(e) => setPassword(e.target.value)}
                        />
                        {error && password.length <= 0 ? (
                          <div style={{ color: "#F53636", marginTop: "0.3rem" }}>
                            Please enter your password !
                          </div>
                        ) : (
                          ""
                        )}
                        <label for="user">password</label>
                      </div>
                    </div>



                    <div className="zone_left_checkbox">
                      <div className="zone_left_input_checkbox">
                        <input
                            type="checkbox"
                            id="Remember"
                            name="Remember"
                            value="Remember"
                          />
                        <div className="Remember">Remember ?</div>
                      </div>
                      <div className="forgot">Forgot password ?</div>
                    </div>



                    <div className="zone_left_button">
                      <GrennButton
                        onClick={onSubmit}
                        GrennButtonName={"SUBMIT"}
                      ></GrennButton>
                    </div>
                  </div>
                </div>
                <div className="zone_right">
                  <div className="zone_right_font">Master Configuration</div>
                  <div>
                    <img src={loginPg} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </FormProvider>
  );
}
