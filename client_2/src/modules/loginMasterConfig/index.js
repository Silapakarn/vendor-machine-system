import { createModule } from 'redux-modux'

const initialState = {
    loginMasterConfigList: [],
    total: null,
    firstLoad: {
        loginMasterConfigList: true,
    },
  }
  
const handlers = {}
export default createModule({ moduleName: 'loginMasterConfig', initialState, handlers })