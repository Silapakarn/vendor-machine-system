import { message, Modal } from 'antd'

export default ({ loadingMessage, successMessage, callAction, onError, onFinally }) =>
  async (dispatch, getState) => {
    let loading = true
    const key = 'fetcherLoading'

    try {
      if (!callAction) return
      if (loadingMessage) {
        message.loading({ content: loadingMessage, key, duration: 60 })
        await callAction(dispatch, getState)
        loading = false
      }

      if (!loadingMessage) {
        await callAction(dispatch, getState)
        loading = false
      }

      if (successMessage) {
        message.success({ content: successMessage, key, duration: 2 })
      }
    } catch (error) {
      const showMessageError = async () => {
       if (error.message === 'undefined undefined')
          await message.error('Something wrong!', 10)
        else await message.error(error.message || 'Something wrong!', 10)
      }
      if (onError) onError()
      if (error.type) {
        message.destroy(key)
        showMessageError()
      } else {
        message.error({ content: 'Service Error', key, duration: 10 })
        console.error(error)
      }
    } finally {
      if (!loading) {
        message.destroy(key)
      }
      if (onFinally) onFinally()
    }
  }