import { useEffect, useState } from "react";
import axios from "axios";

const useFetchGetApi = (url, params) => {
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    axios
      .get(url, params)
      .then((response) => {
        setData(response.data);
      })
      .catch((err) => {
        setError(err);
      }).finally(() => {
        setLoading(false);
      })
  }, [url, params]);

  return { data, error, loading };
}

const useFetchPostApi = (url, params) => {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      setLoading(true);
      axios
        .post(url, params)
        .then((response) => {
          setData(response.data);
        })
        .catch((err) => {
          setError(err);
        }).finally(() => {
          setLoading(false);
        })
    }, [url, params]);  
    return { data, error, loading };
}

const useFetchPutApi = (url, params) => {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      setLoading(true);
      axios
        .put(url, params)
        .then((response) => {
          setData(response.data);
        })
        .catch((err) => {
          setError(err);
        }).finally(() => {
          setLoading(false);
        })
    }, [url, params]);  
    return { data, error, loading };
}

const useFetch = () => {
  let [ errorMessage, setErrorMessage] = useState(null);
  function sendHttpRequest(url, method, body, action){
    fetch(url, {
      method: method,
      body: body ? JSON.stringify(body) : null
    })
    .then((response) => {
      if(!response.ok){
        throw new Error("Something went wrong. Please try again later")
      }
      let data = response.json();
      action(data);
    })
    .catch((error) => {
      setErrorMessage(error.message);
    })
  }
  return [errorMessage, sendHttpRequest]
}

export { useFetchGetApi, useFetchPostApi, useFetchPutApi, useFetch }


