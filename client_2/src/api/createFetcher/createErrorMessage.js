import get from "lodash/get";

const getMessage = (res, options) => {
  if (res.head) return res.head.message;
  if (get(res, "message.value") || get(res, "message.key"))
    return `${res.message.key} ${res.message.description}`;
  if (res.message === "Request failed with status code 401") {
    return "UNAUTHORIZED";
  }
  if (res.message) return res.message;
  if (res.data.status.details.length) return res.data.status.details[0];
  if (res.messages) return res.messages[0];
  if (res.MessageTH) return res.MessageTH;
  if (res.MessageEN) return res.MessageEN;
  if (res.data.messageError) return res.data.messageError;
  if (res.data.response.code === "FAIL")
    return res.data.response["message-tech"] || `Service error`;
  return `Service error`;
};

export const createErrorMessage = (res, options) => {
  throw Object({
    type: "ERROR",
    message: getMessage(res, options),
  });
};

export default createErrorMessage;
