import axios from 'axios'
import createErrorMessage from './createErrorMessage'
import isServiceError from './isServiceError'

const defaultOptions = {
    useMock: false,
    delay: 0,
    jsonMock: '',
    method: 'get',
    url: '',
    params: {},
    data: {},
    timeout: 15000,
    headers: {},
    responseType: 'json',
  }
  
export const createFetcher = ({ fixMenuId = null, ...userOptions }) => {
  
    const options = { ...defaultOptions }
    const { useMock, jsonMock, url, delay, onFailed = () => {} } = options
    const serviceURL = useMock
    options.url = serviceURL
    return new Promise((resolve) => {
        setTimeout(async () => {
          if (useMock) {
            options.method = 'get'
            options.url = serviceURL
          }
          resolve(fetch(options, onFailed, fixMenuId))
        }, delay * 1000)
    })
}

const fetch = async (options, onFailed, fixMenuId) => {
    try{
        options.headers['X-MENU-ID'] = fixMenuId
        const res = await axios(options)
        if (isServiceError(res, options)) {
            createErrorMessage(res, options)
        }
        return res
    }catch(err){
        onFailed(err)
        createErrorMessage(err, options)
    }
}

export default createFetcher
