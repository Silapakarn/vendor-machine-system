export const isServiceError = (res, options) => {
    if (res.status !== 200 && res.status !== 499) return true
    if (res.data.status && res.data.status.code === '102') return true
    return false
  }
  
export default isServiceError
  