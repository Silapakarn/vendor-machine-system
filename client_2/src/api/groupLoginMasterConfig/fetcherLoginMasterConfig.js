import createFetcher from '../createFetcher'
const useMock = false;
const baseUrl = 'http://localhost:8800'


const fetchLoginMasterConfig = (data) =>
  createFetcher({
    useMock,
    method: 'post',
    url: `${baseUrl}/api/auth/token`,
    delay: 0,
    data,
  })


export { fetchLoginMasterConfig }
